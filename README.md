#  **读者须知：**
* 本课程Gitlab 仓库地址： https://gitlab.com/kevin28520/kaggle_iceberg_classifier
* 如果国内用户不能使用Dropbox(我的训练数据集是放到dropbox里面的，然后在云端从dropbox下载训练数据集)，请考虑使用别的方式把训练数据集上传到云端服务器。可能的方式：
    * 1：使用 FileZilla直接拖拽，把训练数据集上传到云端服务器（视频里有演示如何使用FileZilla）。
    * 2：把训练数据集放在其他种类的网盘，创建一个下载链接，最后在云端服务器下载数据集。
* 视频里所用到的所有指令均保存在本仓库里的.py文件里（两个.py文件的开头注释部分）。
* 请结合我的**网易云课堂视频**阅读本文档，**许多细节**无法写在本文档里, 课程名称为**《使用亚马逊云计算训练深度学习模型》**。
* 我的网易云课堂主页：**http://study.163.com/provider/400000000275062/index.htm** 

<img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/WY.JPG">

# 1. 如何使用亚马逊云计算平台(AWS)训练模型

* AWS EC2: https://aws.amazon.com/cn/ec2/?nc1=f_ls
* AWS EC2须知：
    * 价格：https://aws.amazon.com/cn/ec2/pricing/?nc1=h_ls
    * 类型： 
        1. 按需(on demand)，
        2. 竞价型实例(spot instance)，其他。
        3. 详情查看这里：https://aws.amazon.com/cn/ec2/pricing/?nc1=h_ls
    * 按需(on demand)价格：
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/p2g2.JPG">
    * 竞价型实例(spot instance) 价格：
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/spotp2g2.JPG">
    * 如何选择
        1. 强烈建议选择**社区版的实例**，因为在社区版的实例中，深度学习的训练环境（大多数）已经配置好了，
        2. 根据需要选择具体配置，比如g2.2xlarge 或者 p2.xlarge。

    * 假如在某个地区无法创建实例（有限制），请到下面的网址申请提高限制 
        亚马逊技术支持中心，创建请求案例，链接在[这里](https://console.aws.amazon.com/support/v1?region=us-west-2#/case/create?issueType=service-limit-increase&limitType=service-code-ec2-instances),记得要选择正确的地区。


## 使用 AWS EC2
- 我的演示视频使用Windows系统。
- **细节请看视频**，这里只是列举一些重点。

1. 安装Git, https://git-scm.com/downloads
2. 安装FileZilla, https://filezilla-project.org/download.php?type=client
3. 选择合适的服务器地区，如果在某个地区没有使用权限，需要申请开通AWS EC2（视频里有介绍）。
4. 生成 key
5. key 权限
6. 社区版实例与非社区版实例。
7. ssh 链接服务器，如 ssh -i aws_key.pem user@123.123.123.123, 注意这里的 'user'可以是ec2-user, ubuntu, root
参考这里： http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html
8. 其他参考教程：
 - 著名的CS231N课程里的教程：http://cs231n.github.io/aws-tutorial/
 - AWS 官网：http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstances.html
9. 其他指令：
 - `nvidia-smi`：在终端输入次指令并回车，查看NVIDIA显卡参数。
 - `sudo chmod 400 yourkey.pem`: 更改密钥文件（yourkey.pem）的权限。



# 2. Kaggle比赛: Statoil/C-CORE Iceberg Classifier Challenge

* Kaggle 比赛网址: 
    - https://www.kaggle.com/c/statoil-iceberg-classifier-challenge
* 目标: 使用计算机算法自动区分**冰山**与**船**。
* 数据:
    * SAR 数据 （Synthetic-Aperture Radar）: 背景知识请参考维基百科：[Wiki](https://en.wikipedia.org/wiki/Synthetic-aperture_radar)。
    * 图像数据存储在JSON格式文件中。
    * 每张图有两个通道： HH（水平发送/接收）和 HV（水平传输，垂直接收）, [细节](https://www.kaggle.com/c/statoil-iceberg-classifier-challenge#Background)。
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/NM5Eg0Q.png">
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/M8OP2F2.png">
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/M8OP2F2.png">
    * 每个通道图像大小： 75X75
    * 数据类型：float
    * 数据单位：分贝（dB）
    * 一个重要特征：入射角 incidence angle


# 3. 如何在亚马逊AWS上配置 jupyter notebook
Jupyter notebook的细节请看这里：[关于jupyter notebook](http://jupyter.org/)

## 配置步骤：
* 登录AWS EC2, 创建一个AWI实例（略过，请参考上面的创建AWS EC2实例教程）。
    * **注意！**在步骤【配置安全组】时，需要自定义创建一个 TCP 规则以允许从某个端口接入，比如设置端口号为 **8888**，如下图。
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/jupyter%20notebook%20config.JPG">
    
* (**这步可以省略**) 确认你所使用的实例已经安装了jupyter notebook, 演示视频里选择的实例已经安装了jupyter notebook。强烈建议安装Anaconda，因为Anaconda已经包含Python, jupyter notebook以及其他常用库。
    - `nvidia-smi`
    - `sudo pip3 install wget`
    - `sudo wget https://repo.continuum.io/archive/Anaconda3-4.2.0-Linux-x86_64.sh`
    - `bash Anaconda3–4.1.1-Linux-x86_64.sh` (注意，安装的过程中一定要选择把Anaconda3 安装目录添加到 .bashrc PATH 中)
    - 激活安装，输入 `source ~/.bashrc`
    - 测试， 输入 `conda`
    - 安装tensorflow gpu 版本，`conda install -c anaconda tensorflow-gpu`
    - 安装keras:  `conda install -c conda-forge keras`

* 链接到实例。

* 生成一个新的 Jupyter 配置文件, 输入下面指令并回车
    - `jupyter notebook --generate-config`
    - 会看到这样的输出： `Writing default config to: /home/ubuntu/.jupyter/jupyter_notebook_config.py`
    
* 设置 SSL 证书，指令如下
    - `sudo mkdir certificate`
    - `cd certificate`
    
* 使用 OPenSSL 创建新的 SSL 证书
    - `sudo openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem`
    
* 编辑Jupyter notebook 配置文件
    - `sudo nano jupyter_notebook_config.py`
    - 如果打开失败，请尝试使用这条语句 `sudo nano ~/.jupyter/jupyter_notebook_config.py`
    
* 在该文件的开头，插入下面的代码，保存并关闭文件，请参考视频里的操作
    ```python
    c = get_config()
    c.NotebookApp.certfile = u'/home/ubuntu/certificate/mycert.pem'
    c.IPKernelApp.pylab = 'inline'
    c.NotebookApp.ip = '*'
    c.NotebookApp.open_browser = False
    c.NotebookApp.port = 8888
    ```

* 打开服务器端的jupyter notebook
    - 强烈建议使用这句: 输入 `sudo jupyter notebook --allow-root`，回车。
    - 仅仅输入 `jupyter notebook`，可能会遇到权限问题（Running as root is not recommended. Use --allow-root to bypass）。
    
* 拷贝token（拷贝下图中“token=” 后面的部分），第一次连接需要使用
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/tk.JPG">

* 打开浏览器（视频里用的chrome浏览器)
    - 输入`https://ec2-34-208-45-87.us-west-2.compute.amazonaws.com:8888`， 回车。中间一串是你的服务器EC2地址, 在这里找到它：
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/ec2add.JPG">
    
    - 可能会看到警告，点击chrome高级选项，点击继续浏览该地址，如下图：
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/chrome.JPG">

    - 第一次登录界面是这样的，把拷贝的token输入并Log in。
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/login.JPG">

    - 成功登录AWS EC2 上面的 juputer notebook！
    <img src="https://gitlab.com/kevin28520/kaggle_iceberg_classifier/raw/master/samples/afterlogin.JPG">

* 如果任何“权限相关”问题，请使用 `sudo jupyter notebook --allow-root` 打开jupyter notebook。
    
 